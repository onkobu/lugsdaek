# lugsdaek

Combines a directory of snippets into Kubernetes network policies. There are many others out there doing it even life and distributed. Lugsdaek is for the bare metal K8s-rockers and control freaks.

# Sample

In a domain lugsdaek there is a service lugsdaek-service. It consists of pods with a label selector lugsdaek-service-app. The service uses some resources – service' egress. It also allows ingress collecting metrics. In addition ingress from another domain with a certain pod selector is allowed.

```
{
	"domain":"lugsdaek",
	"service":"lugsdaek-service",
	"podSelector":"lugsdaek-service-app",
	"ingress:" [ "Metrics" ],
	"egress":[ "Redis", "Azure SQL" ],
	"ingressFromDomains": [
		{
			"domain":"outskirt",
			"podSelector":"outskirt-service-app"
		}
	]
}
```

Next to this definition there has to be a configuration directory:

```
sample
 |- skeleton.mustache
 |- egress
 |   |- Redis
 |   |- Azure SQL
 |- ingress
     |- Metrics
```

The file skeleton.mustache is filled with the content from egress and ingress directories. An entry from ingress or egress array in the definition's JSON is added under ingress or egress of the network policy. So skeleton.mustache a template with placeholders turns into your fully blown network policy.

```
$> lugsdaek.sh --definition your-definition.json --config sample
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: lugsdaek-service-network-policy
  namespace: 
spec:
  # Normally you should have a pod selector to
  # affect only the service' pods. If not it is
  # up to someone else to do something with {}
  podSelector:
    matchLabels:
    app: lugsdaek-service-app
  policyTypes:
  - Ingress
  - Egress
  ingress:
  # all from same namespace via usual HTTP(s)
  - from:
    - namespaceSelector:
        matchLabels:
          name: 
    ports:
      - protocol: TCP
        port: 8080
      - protocol: TCP
        port: 443
      - protocol: TCP
        port: 80
# ingress from resource Prometheus
  - from:
    - namespaceSelector:
        matchLabels:
          service: prometheus
      podSelector:
        matchLabels:
          app: prometheus
          component: server
    ports:
    - protocol: TCP
      port: 15090
    - protocol: TCP
      port: 8080
    - protocol: TCP
      port: 9191



# ingress from domain outskirt
  - from:
    - namespaceSelector:
        matchLabels:
          service: outskirt
      podSelector:
        matchLabels:
          app: outskirt-service-app

  egress:
  # DNS, if not necessary remove it, if switchable, make
  # it a uses-declaration
  - ports:
    - port: 53
      protocol: UDP
    - port: 53
      protocol: TCP

# egress to resource Redis

  - ports:
    - protocol: TCP
      port: 6380

# egress to resource Azure SQL

  - to:
    - ipBlock:
        cidr: 10.0.0.0/24
    ports:
    - protocol: TCP
      port: 8080

```

All samples provided in this repository are neither the most secure nor the most sophisticated. They may not work for your particular setup. Instead they illustrate the basic principles so you can add ipBlocks, futher podSelectors or whatever your service mesh has to handle.

# Build/ Run

1. get a JDK
1. get Maven
1. `mvn clean install`
1. unpack the ZIP from lugsdaek-app/target anywhere you like
1. run the contained Bash or Batch script -h or --help prints some hints to STDOUT

# Syntax

Only Mustache in skeleton.mustache, with a bit of context trickery in Java. All other files are treated as text/plain and resemble YAML-snippets. Have a look at sample/skeleton.mustache which for example auto-includes comments per snippet to find out the origin of a statement in the final policy.

## Supports

1. header definition in skeleton.mustach
1. replacement of header attributes like service, domain or podSelector
1. iteration over list of resources in uses-section, inserting file content from files in egress directory

## Planned

* processing snippets in ingress-directory
* real world samples with (Azure-) services

# Contribute

File a pull request or use the other means of getting in contact with the project.

# The Fun Part

Lugsdaek is an artificial word I contribute to languages lacking a term for a hideout (German *Versteck* thus sdaek) that enables to oversee unseen (Engl. *to look* or German *lugen*).

Despite this I have absolutely no idea, why YAML is there except that Malbolge needs an offspring that infects minds. In addition:

* correctness depends on the interpreter, there are plenty out there
* the value 1.1 turns into a number, 1.1.1 is a string and "1.1" is a string, too
* watch out for comments, they're not supported everywhere
* get inspired by the trickery around Azure pipelines and its templating – the most suprising way of re-using YAML snippets in YAMLs
* not enough lack of variable replacement, Helm invented another level even with pipes for conversions inspired by Angular
* contexts by indentation, a bad idea for unskilled readers since Python . This is an ongoing discussion and popped up again on the LKML when somebody came up in 2020 with line length limits at 80 characters. Thus YAML is another proof that not only everybody is allowed to write for the public but also does so.
IDEs have a hard job breaking code into human readable groups but whitespaces for indentation aren't necessary at all and could be left out completely. This is an ongoing discussion and popped up again on the LKML when somebody came up in 2020 with line length limits at 80 characters. Thus YAML is another proof that not only everybody is allowed to write for the public but also does so.
