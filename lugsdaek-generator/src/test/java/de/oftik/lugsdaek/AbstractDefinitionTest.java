package de.oftik.lugsdaek;

import static de.oftik.lugsdaek.Lugsdaek.createObjectMapper;
import static de.oftik.lugsdaek.Maps.mapOf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import de.oftik.lugsdaek.RenderDefinitionTest.RenderContextAdapter;

abstract class AbstractDefinitionTest {
	static final RenderContext STANDARD_CONTEXT = new RenderContextAdapter(loadAsString("basic_skeleton.mustache"),
			mapOf("Primary Egress", "Primary Egress Content").put("Secondary Egress", "Secondary Egress Content").end(),
			mapOf("Primary Ingress", "Primary Ingress Content").put("Secondary Ingress", "Secondary Ingress Content")
					.end());

	static Definition load(String defName) throws JsonParseException, JsonMappingException, IOException {
		var om = createObjectMapper();
		return om.readValue(ClassLoader.getSystemResourceAsStream(defName), Definition.class);
	}

	static String loadAsString(String resourceName) {
		return new BufferedReader(
				new InputStreamReader(ClassLoader.getSystemResourceAsStream(resourceName), StandardCharsets.UTF_8))
						.lines().collect(Collectors.joining("\n"));
	}

}
