package de.oftik.lugsdaek;

import java.util.HashMap;
import java.util.Map;

public class Maps {
	static class MapBuilder<K, V> {
		private final Map<K, V> target = new HashMap<>();

		MapBuilder put(K k, V v) {
			target.put(k, v);
			return this;
		}

		Map<K, V> end() {
			return target;
		}
	}

	static <K, V> MapBuilder<K, V> mapOf(K k, V v) {
		return new MapBuilder<>().put(k, v);
	}
}
