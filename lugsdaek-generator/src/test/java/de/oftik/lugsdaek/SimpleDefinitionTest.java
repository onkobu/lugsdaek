package de.oftik.lugsdaek;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

import java.io.IOException;
import java.io.StringWriter;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

final class SimpleDefinitionTest extends AbstractDefinitionTest {
	@Test
	public void parseAndRender() throws JsonParseException, JsonMappingException, IOException {
		var def = load("simple_definition.json");
		final var sw = new StringWriter();
		def.render(STANDARD_CONTEXT, sw);
		assertThat(sw.toString(),
				allOf(containsString("\n  podSelector:{}\n"), Matchers.not(containsString(" Content\n"))));
	}

}
