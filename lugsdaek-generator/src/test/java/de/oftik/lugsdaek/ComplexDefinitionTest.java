package de.oftik.lugsdaek;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.io.StringWriter;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

final class ComplexDefinitionTest extends AbstractDefinitionTest {

	@Test
	public void parseAndRender() throws JsonParseException, JsonMappingException, IOException {
		var def = load("complex_definition.json");
		final var sw = new StringWriter();
		def.render(STANDARD_CONTEXT, sw);
		assertThat(sw.toString(),
				allOf(containsString("80\nPrimary Ingress Content\n"), endsWith("Secondary Egress Content\n")));
	}

}
