package de.oftik.lugsdaek;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.junit.jupiter.api.Test;

final class RenderDefinitionTest {
	private static final String MUSTACHE_RESOURCE_HANDLING = "{{#handleResource}}{{resourceName}}{{/handleResource}}";
	private static final RenderContext CONTEXT_RETURNS_NULL = new RenderContext() {

		@Override
		public Reader getSkeleton() throws IOException {
			return null;
		}

		@Override
		public String findEgressSnippet(String resourceName) throws IOException {
			return null;
		}

		@Override
		public String findIngressSnippet(String valueOf) throws IOException {
			return null;
		}
	};

	static final class RenderContextAdapter implements RenderContext {
		private final String skeleton;
		private final Map<String, String> egressSnippets;
		private final Map<String, String> ingressSnippets;

		RenderContextAdapter(String skeleton, Map<String, String> egressSnippets, Map<String, String> ingressSnippets) {
			this.skeleton = skeleton;
			this.egressSnippets = egressSnippets;
			this.ingressSnippets = ingressSnippets;
		}

		@Override
		public Reader getSkeleton() throws IOException {
			return new StringReader(skeleton);
		}

		@Override
		public String findEgressSnippet(String resourceName) throws IOException {
			return egressSnippets.get(resourceName);
		}

		@Override
		public String findIngressSnippet(String resourceName) throws IOException {
			return ingressSnippets.get(resourceName);
		}
	}

	@Test
	public void nullDefinitionNullContext() throws IOException {
		assertThrows(NullPointerException.class, () -> new Definition(null, null, null, null, null, null).render(null));
	}

	@Test
	public void nullDefinitionContextReturnsNull() throws IOException {
		final var sw = new StringWriter();
		new Definition(null, null, null, null, null, null).render(CONTEXT_RETURNS_NULL, sw);
		assertThat(sw.toString(), emptyString());
	}

	@Test
	public void emptyDefinitionEmptySkeleton() throws IOException {
		final var sw = new StringWriter();
		new Definition(null, null, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList())
				.render(new RenderContextAdapter("", Collections.emptyMap(), Collections.emptyMap()), sw);
		assertThat(sw.toString(), emptyString());
	}

	@Test
	public void staticDefinitionEmptySkeleton() throws IOException {
		final var sw = new StringWriter();
		new Definition(null, null, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList())
				.render(new RenderContextAdapter("static", Collections.emptyMap(), Collections.emptyMap()), sw);
		assertThat(sw.toString(), equalTo("static"));
	}

	@Test
	public void domainNameDefinition() throws IOException {
		final var sw = new StringWriter();
		final var expectedValue = "domainName";
		new Definition(expectedValue, null, null, Collections.emptyList(), Collections.emptyList(),
				Collections.emptyList()).render(
						new RenderContextAdapter("{{domain}}", Collections.emptyMap(), Collections.emptyMap()), sw);
		assertThat(sw.toString(), equalTo(expectedValue));
	}

	@Test
	public void podSelectorDefinition() throws IOException {
		final var sw = new StringWriter();
		final var expectedValue = "thePodSelector";
		new Definition("domainName", expectedValue, null, Collections.emptyList(), Collections.emptyList(),
				Collections.emptyList()).render(
						new RenderContextAdapter("{{podSelector}}", Collections.emptyMap(), Collections.emptyMap()),
						sw);
		assertThat(sw.toString(), equalTo(expectedValue));
	}

	@Test
	public void serviceDefinition() throws IOException {
		final var sw = new StringWriter();
		final var expectedValue = "testService";
		new Definition("domainName", "thePodSelector", expectedValue, Collections.emptyList(), Collections.emptyList(),
				Collections.emptyList()).render(
						new RenderContextAdapter("{{service}}", Collections.emptyMap(), Collections.emptyMap()), sw);
		assertThat(sw.toString(), equalTo(expectedValue));
	}

	@Test
	public void ingressDefinition() throws IOException {
		final var sw = new StringWriter();
		final var expectedContent = "Basic Content";
		final var resourceName = "Basic Service";
		new Definition("domainName", "thePodSelector", "testService", Arrays.asList(new IngressResource(resourceName)),
				Collections.emptyList(), Collections.emptyList())
						.render(new RenderContextAdapter("{{#ingress}}" + MUSTACHE_RESOURCE_HANDLING + "{{/ingress}}",
								Collections.emptyMap(), Maps.mapOf(resourceName, expectedContent).end()), sw);
		assertThat(sw.toString(), equalTo(expectedContent));
	}

	@Test
	public void egressDefinition() throws IOException {
		final var sw = new StringWriter();
		final var expectedContent = "Basic Content";
		final var resourceName = "Basic Service";
		new Definition("domainName", "thePodSelector", "testService", Collections.emptyList(),
				Arrays.asList(new EgressResource(resourceName)), Collections.emptyList())
						.render(new RenderContextAdapter("{{#egress}}" + MUSTACHE_RESOURCE_HANDLING + "{{/egress}}",
								Maps.mapOf(resourceName, expectedContent).end(), Collections.emptyMap()), sw);
		assertThat(sw.toString(), equalTo(expectedContent));
	}
}
