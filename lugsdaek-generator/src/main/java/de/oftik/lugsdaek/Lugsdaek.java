package de.oftik.lugsdaek;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class Lugsdaek implements RenderContext {

	enum CliOption {
		help("h", false, "Print this help and exit"),

		config("c", true, "Configuration directory"),

		definition("d", true, "Policy definition file");

		private final String shortOption;
		private final boolean argument;
		private final String description;

		CliOption(String shortOption, boolean argument, String description) {
			this.shortOption = shortOption;
			this.argument = argument;
			this.description = description;
		}

		boolean isPresent(CommandLine cli) {
			return cli.hasOption(shortOption);
		}

		Option toOption() {
			return Option.builder(shortOption).longOpt(name()).desc(description).hasArg(argument).build();
		}

		static Options createOptions() {
			final Options options = new Options();
			for (CliOption o : values()) {
				options.addOption(o.toOption());
			}
			return options;
		}

		String valueOrDefault(CommandLine cli, String defValue) {
			if (!cli.hasOption(shortOption)) {
				return defValue;
			}
			return cli.getOptionValue(shortOption);
		}
	}

	private static final Options OPTIONS = CliOption.createOptions();

	private final File configRoot;
	private final File ingressRoot;
	private final File egressRoot;

	private Lugsdaek(String configRoot) {
		this.configRoot = new File(configRoot);
		this.ingressRoot = new File(configRoot, "ingress");
		this.egressRoot = new File(configRoot, "egress");
	}

	public static void main(String[] args) throws Exception {
		final CommandLineParser p = new DefaultParser();
		final CommandLine cli = p.parse(OPTIONS, args);

		if (CliOption.help.isPresent(cli)) {
			printHelpAndExit();
			return;
		}

		final var mapper = createObjectMapper();
		final var defn = mapper.readValue(new File(CliOption.definition.valueOrDefault(cli, "definition.json")),
				Definition.class);
		System.out.println("Found definition: " + defn);
		defn.render(new Lugsdaek(CliOption.config.valueOrDefault(cli, "sample")));
	}

	static ObjectMapper createObjectMapper() {
		return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static void printHelpAndExit() {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("clock.Clock", OPTIONS);

		System.exit(0);
	}

	@Override
	public Reader getSkeleton() throws IOException {
		return new BufferedReader(new FileReader(new File(configRoot, "skeleton.mustache")));
	}

	@Override
	public String findEgressSnippet(String resourceName) throws IOException {
		return Files.readString(new File(egressRoot, resourceName).toPath());
	}

	@Override
	public String findIngressSnippet(String resourceName) throws IOException {
		return Files.readString(new File(ingressRoot, resourceName).toPath());
	}
}
