package de.oftik.lugsdaek;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.mustachejava.DefaultMustacheFactory;

class Definition {
	private static final Logger logger = Logger.getLogger(Definition.class.getName());

	private final String domain;
	private final String podSelector;
	private final String service;

	private final List<EgressResource> egress;
	private final List<IngressResource> ingress;

	private final List<DomainReference> ingressFromDomains;

	@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
	public Definition(@JsonProperty("domain") String domain, @JsonProperty("podSelector") String podSelector,
			@JsonProperty("service") String service, @JsonProperty("ingress") List<IngressResource> ingress,
			@JsonProperty("egress") List<EgressResource> egress,
			@JsonProperty("ingressFromDomains") List<DomainReference> ingressFromDomains) {
		super();
		this.domain = domain;
		this.podSelector = podSelector;
		this.service = service;
		this.ingress = ingress;
		this.egress = egress;
		this.ingressFromDomains = ingressFromDomains;
	}

	String getDomain() {
		return domain;
	}

	String getPodSelector() {
		return podSelector;
	}

	String getService() {
		return service;
	}

	List<EgressResource> getEgress() {
		return egress;
	}

	List<IngressResource> getIngress() {
		return ingress;
	}

	List<DomainReference> getIngressFromDomains() {
		return ingressFromDomains;
	}

	@Override
	public String toString() {
		return getDomain() + ": " + getEgress().size() + " egress snippets used, " + getIngress().size()
				+ " ingress snippets used, ingress from " + getIngressFromDomains().size() + " other domains";
	}

	public void render(RenderContext renderContext) throws IOException {
		render(renderContext, new PrintWriter(System.out));
	}

	public void render(RenderContext renderContext, Writer output) throws IOException {
		if (renderContext.getSkeleton() == null) {
			logger.severe("Cannot render without a skeleton");
			return;
		}
		if (getIngress() != null) {
			getIngress().stream().forEach(r -> r.prepare(renderContext));
		}
		if (getEgress() != null) {
			getEgress().stream().forEach(r -> r.prepare(renderContext));
		}

		final var mf = new DefaultMustacheFactory();
		final var m = mf.compile(renderContext.getSkeleton(), "skeleton");
		m.execute(output, this).flush();
	}

}