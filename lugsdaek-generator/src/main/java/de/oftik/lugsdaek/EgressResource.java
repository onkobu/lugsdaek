package de.oftik.lugsdaek;

import java.io.IOException;
import java.util.function.Function;

final class EgressResource {
	private final String resourceName;
	private Function<Object, Object> handleResource;

	public EgressResource(String resourceName) {
		super();
		this.resourceName = resourceName;
	}

	String getResourceName() {
		return resourceName;
	}

	@Override
	public String toString() {
		return getResourceName();
	}

	Function<Object, Object> getHandleResource() {
		return handleResource;
	}

	public void prepare(RenderContext lugsdaek) {
		handleResource = obj -> {
			try {
				return lugsdaek.findEgressSnippet(String.valueOf(obj));
			} catch (IOException e) {
				return "# could not find " + obj;
			}
		};
	}

}