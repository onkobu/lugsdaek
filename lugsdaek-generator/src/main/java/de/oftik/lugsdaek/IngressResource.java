package de.oftik.lugsdaek;

import java.io.IOException;
import java.util.function.Function;

final class IngressResource {
	private final String resourceName;
	private Function<Object, Object> handleResource;

	public IngressResource(String resourceName) {
		super();
		this.resourceName = resourceName;
	}

	String getResourceName() {
		return resourceName;
	}

	@Override
	public String toString() {
		return getResourceName();
	}

	Function<Object, Object> getHandleResource() {
		return handleResource;
	}

	public void prepare(RenderContext lugsdaek) {
		handleResource = obj -> {
			try {
				return lugsdaek.findIngressSnippet(String.valueOf(obj));
			} catch (IOException e) {
				return "# could not find " + obj;
			}
		};
	}

}