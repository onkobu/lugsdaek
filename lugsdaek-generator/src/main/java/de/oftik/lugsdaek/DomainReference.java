package de.oftik.lugsdaek;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class DomainReference {
	private final String domain;
	private final String podSelector;

	@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
	public DomainReference(@JsonProperty("domain") String domain, @JsonProperty("podSelector") String podSelector) {
		super();
		this.domain = domain;
		this.podSelector = podSelector;
	}

	String getDomain() {
		return domain;
	}

	String getPodSelector() {
		return podSelector;
	}

}