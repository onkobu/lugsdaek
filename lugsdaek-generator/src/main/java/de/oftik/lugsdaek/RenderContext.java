package de.oftik.lugsdaek;

import java.io.IOException;
import java.io.Reader;

public interface RenderContext {

	Reader getSkeleton() throws IOException;

	String findEgressSnippet(String resourceName) throws IOException;

	String findIngressSnippet(String resourceName) throws IOException;

}
